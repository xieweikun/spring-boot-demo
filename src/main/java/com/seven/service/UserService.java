package com.seven.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seven.model.User;

public interface UserService extends IService<User> {

    boolean registryUser(User dto);

    String login(User dto);

    User checkToken(String token);

}
