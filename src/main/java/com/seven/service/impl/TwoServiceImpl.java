package com.seven.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seven.mapper.TwoMapper;
import com.seven.model.Two;
import com.seven.service.TwoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwoServiceImpl extends ServiceImpl<TwoMapper, Two> implements TwoService {

    @Autowired
    private TwoMapper twoMapper;

    @Override
    public Two getTwoByTKey(String tKey) {
        Two twoByTKey = twoMapper.getTwoByTKey(tKey);
        return twoByTKey;
    }

}
