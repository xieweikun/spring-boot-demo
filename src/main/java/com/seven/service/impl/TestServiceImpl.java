package com.seven.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seven.mapper.TestMapper;
import com.seven.model.Test;
import com.seven.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: TestServiceImpl
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/14 15:05
 * @Version 1.0
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Test getTestByTKey(String tKey) {
        Test testByTKey = testMapper.getTestByTKey(tKey);
        return testByTKey;
    }

}
