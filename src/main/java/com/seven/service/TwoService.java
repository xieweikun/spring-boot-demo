package com.seven.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seven.model.Two;

public interface TwoService extends IService<Two> {

    Two getTwoByTKey(String tKey);

}
