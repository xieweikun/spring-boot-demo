package com.seven.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seven.model.Test;

/**
 * @Title: TestService
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/14 15:05
 * @Version 1.0
 */
public interface TestService extends IService<Test> {

    Test getTestByTKey(String tKey);

}
