package com.seven.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * @Title: Test
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/14 14:54
 * @Version 1.0
 */
@Data
@TableName("test")
public class Test {

    /** id */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /** 键 */
    private String tKey;

    /** 值 */
    private String tValue;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

}
