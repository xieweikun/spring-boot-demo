package com.seven.model;

/**
 * @Title: Result
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/27 17:31
 * @Version 1.0
 */
public class Result {

    private int code;
    private String message;
    private Object result;

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Object getResult() {
        return result;
    }
    public void setResult(Object result) {
        this.result = result;
    }

    public Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.result = data;
    }

}
