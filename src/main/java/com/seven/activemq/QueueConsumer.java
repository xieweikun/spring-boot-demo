package com.seven.activemq;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumer {

    /**
     * 监听消息,名字为生产者发送的名字,要一致,不然监听不到.
     * 因为是队列模式,只能消费者
     * @param string
     */
    @JmsListener(destination = "queue_mq")
    public void consumerQueue(String string){
        System.out.println("消费消息成功,信息为---> "+string);
    }

}