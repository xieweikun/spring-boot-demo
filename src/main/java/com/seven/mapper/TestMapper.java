package com.seven.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seven.model.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Title: TestMapper
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/14 16:07
 * @Version 1.0
 */
@Mapper
public interface TestMapper extends BaseMapper<Test> {

    Test getTestByTKey(@Param("tKey") String tKey);

}
