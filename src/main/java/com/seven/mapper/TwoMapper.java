package com.seven.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seven.model.Two;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Title: TestMapper
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/14 16:07
 * @Version 1.0
 */
@DS("slave_1")
@Mapper
public interface TwoMapper extends BaseMapper<Two> {

    Two getTwoByTKey(@Param("tKey") String tKey);

}
