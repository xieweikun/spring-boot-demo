package com.seven.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.jms.Queue;

/**
 * @Title: ProducerController
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/15 17:31
 * @Version 1.0
 */
@RestController
public class ProducerController {

    @Autowired
    private Queue queue;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;


    /**
     * 点对点的消息队列的生产者
     * @param string
     */
    @RequestMapping(value = "/producer/queue/{string}", method = RequestMethod.GET)
    public void sendMsgQueue(@PathVariable("string") String string){
        System.out.println("消息已经发送,准备被消费,消息为 ---> "+string);
        jmsMessagingTemplate.convertAndSend(queue,string);
    }


}
