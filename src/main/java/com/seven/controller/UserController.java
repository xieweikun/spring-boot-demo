package com.seven.controller;

import com.seven.config.ResultFactory;
import com.seven.model.Result;
import com.seven.model.User;
import com.seven.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/user/registry")
    public Result registryUser(@RequestBody User dto){
        return ResultFactory.buildSuccessResult(userService.registryUser(dto));
    }

    @PostMapping("/login")
    public Result login(@RequestBody User dto) {
        return ResultFactory.buildSuccessResult(userService.login(dto));
    }

}
