package com.seven.controller;

import com.seven.model.Two;
import com.seven.service.TwoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwoController {

    @Autowired
    TwoService twoService;

    @RequestMapping(value = "/two/getTwoByTKey/{tKey}", method = RequestMethod.GET)
    public Two getTwoByTKey(@PathVariable("tKey") String tKey){
        Two two = twoService.getTwoByTKey(tKey);
        return two;
    }

}
