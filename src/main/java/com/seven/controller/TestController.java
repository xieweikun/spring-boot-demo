package com.seven.controller;

import com.seven.model.Test;
import com.seven.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    TestService testService;
    @Autowired
    RedisTemplate redisTemplate;


    @RequestMapping(value = "/test/getTestByTKey/{tKey}", method = RequestMethod.GET)
    public Test getTestByTKey(@PathVariable("tKey") String tKey){
        Test test = testService.getTestByTKey(tKey);
        return test;
    }

    @RequestMapping(value = "/test/getRedisValue", method = RequestMethod.GET)
    public Object getRedisValue(){
        redisTemplate.opsForValue().set("name", "seven");
        Object obj = redisTemplate.opsForValue().get("name");
        return obj;
    }

}
