package com.seven.enums;

/**
 * @Title: ResultEnum
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/27 17:28
 * @Version 1.0
 */
public enum ResultEnum {

    SUCCESS(200),
    FAIL(400),
    UNAUTHORIZED(401),
    NOT_FOUND(404),
    INTERNAL_SERVER_ERROR(500);

    public int code;

    ResultEnum(int code) {
        this.code = code;
    }
}
