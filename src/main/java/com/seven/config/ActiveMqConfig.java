package com.seven.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

import javax.jms.Queue;

/**
 * @Title: ActiveMqConfig
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/15 16:27
 * @Version 1.0
 */
@Configuration
@EnableJms
public class ActiveMqConfig {

    /**
     * 创建点对点的队列  一个消息只能被一个消费者消费  --- 一对一
     * @return
     */
    @Bean
    public Queue queue(){
        return new ActiveMQQueue("queue_mq");
    }

}
