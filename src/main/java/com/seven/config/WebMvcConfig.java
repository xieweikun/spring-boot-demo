package com.seven.config;

import com.seven.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Title: WebMvcConfig
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/27 17:49
 * @Version 1.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer{

    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/test/**")
                .addPathPatterns("/admin/**");
    }

}
