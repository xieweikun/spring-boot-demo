package com.seven.config;

import com.seven.enums.ResultEnum;
import com.seven.model.Result;

/**
 * @Title: ResultFactory
 * @Description: TODO
 * @Author 谢伟锟
 * @Date 2023/12/27 17:32
 * @Version 1.0
 */
public class ResultFactory {

    public static Result SUCCESS_RESULT = new Result(200,"SUCCESS",null);
    public static Result FAIL_RESULT = new Result(400,"FAIL",null);

    public static Result buildSuccessResult(Object data) {
        return buildResult(ResultEnum.SUCCESS, "成功", data);
    }

    public static Result buildFailResult(String message) {
        return buildResult(ResultEnum.FAIL, message, null);
    }

    public static Result buildResult(ResultEnum resultEnum, String message, Object data) {
        return buildResult(resultEnum.code, message, data);
    }

    public static Result buildResult(int resultCode, String message, Object data) {
        return new Result(resultCode, message, data);
    }
}
