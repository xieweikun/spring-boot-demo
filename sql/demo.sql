DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL COMMENT '用户名',
    `password` varchar(255) NOT NULL COMMENT '密码',
    `salt` varchar(255) NOT NULL COMMENT '盐',
    `age` int(3) NOT NULL COMMENT '年龄',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
    `id` int NOT NULL AUTO_INCREMENT,
    `menu_path` varchar(255) NOT NULL COMMENT '路径',
    `menu_code` varchar(255) NOT NULL COMMENT '菜单code',
    `menu_icon` varchar(255) NOT NULL COMMENT '菜单图标',
    `menu_name` varchar(255) NOT NULL COMMENT '菜单名称',
    `parent_id` int NOT NULL COMMENT '父id',
    `menu_sort` int DEFAULT NULL COMMENT '排序',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
    `id` int NOT NULL AUTO_INCREMENT,
    `role_code` varchar(255) NOT NULL COMMENT '角色code',
    `role_name` varchar(255) NOT NULL COMMENT '角色名',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

DROP TABLE IF EXISTS `conn_role_user`;
CREATE TABLE `conn_role_user` (
    `id` int NOT NULL AUTO_INCREMENT,
    `role_id` int NOT NULL COMMENT '角色id',
    `user_id` int NOT NULL COMMENT '用户id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色用户关联表';

DROP TABLE IF EXISTS `conn_role_menu`;
CREATE TABLE `conn_role_menu` (
      `id` int NOT NULL AUTO_INCREMENT,
      `role_id` int NOT NULL COMMENT '角色id',
      `menu_id` int NOT NULL COMMENT '菜单id',
      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单关联表';






